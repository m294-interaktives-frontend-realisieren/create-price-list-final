function createRow(tagCell, cellValues) {
  const elTr = document.createElement('tr');
  // Zellen Elemente erzeugen, mit Wert befüllen
  // und Zeilen Element hinzufügen
  for (let i = 0; i < cellValues.length; i++) {
    const value = cellValues[i];
    // Für Head th-Element, für Body td-Element erzeugen
    let elCell = document.createElement(tagCell);
    elCell.textContent = value;
    elTr.appendChild(elCell);
  }
  return elTr;
}

function createTdValues(qty, products) {
  values = [qty];
  // Preise für alle Produkte berechnen
  for (let i = 0; i < products.length; i++) {
    // Preis für entsprechende Produkt Anz. berechnen
    const value = qty * products[i].price;
    values.push(value);
  }
  return values;
}

function createThValues(products) {
  // Titel 1. Spalte ist Anzahl
  values = ['Anzahl'];
  // Namen aller Produkte als Titel weiterer Spalten hinzufügen
  for (let i = 0; i < products.length; i++) {
    const name = products[i].product;
    values.push(name);
  }
  return values;
}

function createPriceListTable(nrOfEntries, products) {
  const elTable = document.createElement('table');
  // thead-Element mit Titel Zellen erzeugen und
  // table-Element hinzufügen
  const elTHead = document.createElement('thead');
  let elRow = createRow('th', createThValues(products));
  elTHead.appendChild(elRow);
  elTable.appendChild(elTHead);
  // tbody-Element mit Titel Zellen erzeugen und
  // table-Element hinzufügen
  const elTBody = document.createElement('tbody');
  for (let qty = 1; qty <= nrOfEntries; qty++) {
    elRow = createRow('td', createTdValues(qty, products));
    elTBody.appendChild(elRow);
  }
  elTable.appendChild(elTBody);
  return elTable;
}

let elBody = document.querySelector('body');
let elScript = document.querySelector('body script');
let priceList = createPriceListTable(7, [
  { product: 'Kanone Deluxe', price: 5400 },
  { product: 'Flammenwerfer', price: 400 },
  { product: 'Feuerlöscher', price: 150 },
]);
elBody.insertBefore(priceList, elScript);
